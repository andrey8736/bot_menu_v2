<?php

function indexMenu($menu, $idx='')
{
    if ($idx) $idx .= ".";
    $items = null;
    $i = 0;

    foreach ($menu as $k => $v) {
		$i += 1;
		if (isset($v['items'])) {
		    $v['menu'] = true;
		    $v["items"] = indexMenu($v["items"], "${idx}${i}");
		} else {
		    $v['menu'] = false;
		}
		$items[$k] = $v;
		$items[$k]['idx'] = "${idx}${i}";
		if (! isset($items[$k]['actions'])) {
		    $items[$k]['actions'] = [];
		}
    }
    return $items;
}


function loadMenu($file_name="./menu.json")
{
	$m = json_decode(
		file_get_contents($file_name),
		$assoc=true
	);
	if (json_last_error() != JSON_ERROR_NONE) {
		return null;
	}
	return indexMenu($m);
}


function getMenu($menu, $idx)
{
    reset($menu);	
    $menu_caption = key($menu);
    if ($menu[$menu_caption]['idx'] == $idx) {
	return $menu;
    }

    if ($menu[$menu_caption]['menu']) {
	foreach ($menu[$menu_caption]['items'] as $k => $i) {
	    if ($m = getMenu([$k=>$i], $idx)) return $m;
	}
    }
    return null;
}


function getPath($menu, $idx, $sep=' -> ')
{
    $path = '';
//	if ($path) $path .= $sep;
    // $p .= $menu_caption
    
    reset($menu);	
    $menu_caption = key($menu);
    if ($menu[$menu_caption]['idx'] == $idx) {
	return $menu_caption;
    }

    if ($menu[$menu_caption]['menu']) {
	foreach ($menu[$menu_caption]['items'] as $k => $i) {
	    if ($p = getPath([$k=>$i], $idx)) {
		return $menu_caption.$sep.$p;
	    }
	}
    }
    return '';
}


function doActions($actions)
{
    foreach ($actions as $a => $p){
		switch ($n) {
		    case 'send_message':

			break;
		    case "get_message":

			break;
		}
    }   
}
