<?php

use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;

class NewMessage
{
	public $text = '';
	public $chat_id = '';
	public $buttons = null;
	public $keyboard = null;
	public $force_reply = false;
	public $chat_instance = '';
	public $response = null;

	private $action = '';


	private function getMenuButtons($menu, $prev='')
	{
		$buttons = [];

		// Prev button
		if ($prev != '1'){
			$buttons[] = [
				'text'=>"<",
				'callback_data'=>$prev,
			];
		}
		foreach ($menu as $menu_caption=>$item) {

			$buttons[] = [
				'text'=>"$menu_caption",
				'callback_data'=>"".$item['idx']
			];
		}
		return [$buttons];
	}


	public function makeMenu($menu)
	{
		if (!$menu) return "Menu is empty";

		reset($menu);
		$this->text = key($menu);
		if (! $menu[$this->text]["menu"]){
			//$this->$action = '';
			return "There is no menu items";
		}
		$this->buttons = $this->getMenuButtons(
			$menu[$this->text]["items"],
			$menu[$this->text]["idx"],
		);

		return '';
	}


	public function sendMessage($tg)
	{
		if ($this->buttons) {
			$this->keyboard = Keyboard::make([
				'inline_keyboard' => $this->buttons,
				'resize_keyboard' => true,
				'one_time_keyboard' => true,
				'inline' => true,
			]);
		} else {
			$this->keyboard = Keyboard::make([
				'force_reply' => $this->force_reply,
			]);
		}

		$this->response = $tg->sendMessage([
			'chat_id' => $this->chat_id, 
			'text' => $this->text,
			'reply_markup' => $this->keyboard,
			'chat_instance' => $this->chat_instance
		]);
		return $this->response;
	}
}
