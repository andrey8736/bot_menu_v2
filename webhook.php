<?php
include('./vendor/autoload.php');
include('config.php');
include('NewMessage.php');
include('Menu.php');
// include('./Logger.php');

use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;


$menu = loadMenu("./menu.json");

$telegram = new Api($BOT_TOKEN);

$data = $telegram->getWebhookUpdates();

$new_message = new NewMessage();

if (isset($data['message'])) {
	$message = $data['message'];
	$from = $data['message']['from'];
	$data = null;

	// От бота боту сообщения не рассматриваем
	if ($from['is_bot']) exit();
	
	// Это ответ на предыдущее сообщение?
	if (isset($message["reply_to_message"])) {
		// Отправляем сообщение в другой чат
		if ($ADMIN_CHAT_ID) {
			$idx = null;
			preg_match('/^<([\d\.]*)>/',
						$message["reply_to_message"]['text'],
						$idx);
			if ($idx) {
				$menu_path = getPath($menu, $idx[1])."\n";
			} else {
				$menu_path = "\n";
			}
			$new_message->text = ""
				."Сообщение из чата "
				.$message["reply_to_message"]['chat']['title']
				." [".$message["reply_to_message"]['chat']['id']."]"
				."\n"
				.$menu_path
				.'@'.$from['username'].":\n"
				.$message["reply_to_message"]['text']
				."\n"
				.$message['text'];
			$new_message->chat_id = $ADMIN_CHAT_ID;
			$new_message->sendMessage($telegram);
		}

		$new_message->chat_id = $message['chat']['id'];
		$new_message->text = "Ваш запрос отправлен.";
		$new_message->sendMessage($telegram);
	} else {
		if ($new_message->makeMenu($menu)) exit();
		$new_message->chat_id = $message['chat']['id'];
		$new_message->sendMessage($telegram);
	}

} elseif (isset($data['callback_query'])) {
	// Callback

	$message = $data['callback_query']['message'];
	$from = $data['callback_query']['from'];
	$callback_data = $data['callback_query']['data'];
	$chat_instance = $data['callback_query']['chat_instance'];
	
	$data = null;

	// От бота боту сообщения не обрабатываем
	if ($from['is_bot']) return;
	
	// Актуален ли данный пункт меню
	if (! $cur_menu = getMenu($menu, $callback_data)) exit();

	$cur_menu_key = key($cur_menu);
	
	$new_message->chat_id = $message['chat']['id'];;
	$new_message->chat_instance = $chat_instance;

	// Удаляем предыдущее собщение
	$telegram->deleteMessage([
			'chat_id' => $message['chat']['id'],
			'message_id' => $message['message_id']
	]);

	if ($cur_menu[$cur_menu_key]['menu']) {
		// Следующее меню
		if ($new_message->makeMenu($cur_menu)) exit();
		$new_message->sendMessage($telegram);
	} else {
		// Запрос сообщения
		// if ($cur_menu['actions']) {}
		$new_message->text = ""
			."<".$cur_menu[$cur_menu_key]['idx']."> "
			//.getPath($menu, $callback_data)
			."\nCообщение:";

		$new_message->force_reply = true;
		$new_message->sendMessage($telegram);
	}

} else {
	exit();
}
